import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutterlojavirtual/datas/product_data.dart';

class CartProduct {
  String cid;
  String category;
  String pid;
  int quantity;
  String size;
  ProductData productData;

  CartProduct.fromDocument(DocumentSnapshot doc) {
    cid = doc.documentID;
    category = doc.data["category"];
    pid = doc.data["pid"];
    quantity = doc.data["quantity"];
    size = doc.data["size"];
  }

  CartProduct();

  Map<String, dynamic> toMap() {
    return {
      "category": category,
      "pid": pid,
      "quantity": quantity,
      "size": size,
      "product": productData.toResumeMap()
    };
  }

}